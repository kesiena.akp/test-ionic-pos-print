# test ionic pos printer

## Overview
Simple app to demonstrate and test integrating an Ionic app with the POS printer

## Prerequisites
- NodeJS/NPM
- Ionic CLI (run sudo npm -i ionic@latest)
- Open mind and some time

## Instalation
- clone this repo
- run `sudo ionic platform add android` to add the Android platform
- connect your Android device the system
- run `sudo ionic run android` to install and launch the app
- **ideally, clicking the print button, should print some content, that's what needs figuring out`**
